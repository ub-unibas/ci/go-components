package main

import (
	"testing"
)

func TestXPlusOne(t *testing.T) {
	got := XPlusOne(1)
	want := 2
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
