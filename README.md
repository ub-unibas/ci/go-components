# GitLab CI/CD Components

[CI/CD components](https://docs.gitlab.com/ee/ci/components/) are reusable
single pipeline configuration units. This repository contains such units used
in projects maintained by the IT department of the [University Library of Basel](https://ub.unibas.ch).

## Components

### test-unit

Performs unit tests

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Artifacts

- JUnit test reports in `target/test-reports`
- Test reports in pipeline

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/test-unit@main
    inputs:
      internal_libs: true
```


### test-lint

Performs linting

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/test-lint@main
    inputs:
      internal_libs: true
```


### test-race

Performs data race detection

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/test-lint@main
    inputs:
      internal_libs: true
```


### test-msan

Performs memory sanity checks

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/test-lint@main
    inputs:
      internal_libs: true
```


### test-coverage

Creates a test coverage report. Additionally, it extracts the information required for generating a [coverage badge](https://docs.gitlab.com/ee/user/project/badges.html#test-coverage-report-badges).

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Artifact

Coverage report in `coverage-report.html`

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/test-unit@main
    inputs:
      internal_libs: true
```


### build

Builds Go application

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"build"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Artifact

Binary application in `$HOME/app`

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/build@main
    inputs:
      internal_libs: true
```


### pages

Creates and publishes documentation as GitLab pages

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `project_main` | Path to the main package | `string` | - |
| `stage` | Stage in CI/CD | `string` | `"build"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.23.1` |

#### Artifact

Documentation in folder `public/`

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/pages@main
    inputs:
      internal_libs: true
```


### go-default

Runs all components with default values. Requires stages `test`, `build` and
`deploy` per default.

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/go-components/go-default@main
```
