// Just the main package
package main

import "fmt"

func main() {
	fmt.Println(XPlusOne(2))
}

// This adds 1 to x
func XPlusOne(x int) int {
	return x + 1
}
